<?php
require_once __DIR__ . '/vendor/autoload.php';

use EditeurLogiciel\Developpeur;
use EditeurLogiciel\Equipe;

$arthur = new Developpeur('Arthur');
$arthur->devenirSpecialiste('ops');
$franck = new Developpeur('Franck');
$franck->devenirSpecialiste('front');
$frederic = new Developpeur('Frédéric');
$mohamed = new Developpeur('Mohamed');
$mohamed->devenirSpecialiste('chercheur');
$raphael = new Developpeur('Raphael');
$raphael->devenirSpecialiste('ops');
$thibault = new Developpeur('Thibault');
$thibault->devenirSpecialiste('po');

$equipeLama = new Equipe('LAMA');
$equipeLama->addEquipier($arthur);
$equipeLama->addEquipier($franck);
$equipeLama->addEquipier($frederic);
$equipeLama->addEquipier($mohamed);
$equipeLama->addEquipier($thibault);

$lama = $equipeLama->fabriquerLogiciel();
/*$lama->developper();
$lama->deployerCode();
$lama->assurerMaintenance();
*/
/*



class Developpeur extends Personne {

}


class ProductOwner extends Personne {

}

*/