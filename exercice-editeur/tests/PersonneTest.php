<?php

namespace EditeurLogiciel\Test;

use PHPUnit\Framework\TestCase;
use EditeurLogiciel\Personne;

class PersonneTest extends TestCase
{
    public function testConstruct() {
        $personne = new Personne('Cleopatra', 'Septia');
        $this->assertInstanceOf(Personne::class, $personne);
    }
}
