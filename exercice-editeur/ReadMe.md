(code d'exemple utilisé pour les exercices dans le cadre de la formation "codons proprement")

EditeurLogiciel est un logiciel qui permet de créer des logiciels:
* former une équipe
* fournir un cahier des charges
* créer le logiciel
* deployer le logiciel
* maintenir le logiciel

# Installation

* Récuperer EditeurLogiciel, et lancer composer install

```
git clone git@gitlab.com:fredericlog/codonsproprement.git
cd codonsproprement/exercice-editeur
bin/qa composer install
```

* démonstration (dans un terminal, se placer à la racine du projet)

`bin/qa php main.php`

# Tests
Pour lancer les tests unitaire, dans un terminal à la racine du projet:

`bin/qa phpunit tests`

