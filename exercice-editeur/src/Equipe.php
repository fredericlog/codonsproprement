<?php
namespace EditeurLogiciel;


class Equipe {
    private string $name;

    /** @var array<int,Personne> */
    private array $membres;

    public function __construct(string $name) {
        $this->name = $name;
    }

    public function addEquipier(Personne $personne):void
    {
        $this->membres[] = $personne;
    }

    public function fabriquerLogiciel():void {
        for($i=0; $i<sizeof($this->membres); $i++) {
            $this->membres[$i]->sayReady();
        }
    }
}