<?php
namespace EditeurLogiciel;

class Developpeur extends Personne {

    /** @var array<int, string> */
    private array $competences;

    public function __construct(string $prenom, string $nom = '') {
        parent::__construct($prenom, $nom);
        $this->competences[] = 'developpeur';
    }

    public function devenirSpecialiste(string $competence):void
    {
        switch($competence) {
            case 'ops':
            case 'front':
            case 'chercheur':
                $this->competence[] = $competence;
                break;
        }
    }
}
