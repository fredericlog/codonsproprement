<?php
namespace EditeurLogiciel;

class Personne {
    private string $nom;
    private string $prenom;

    public function __construct(string $prenom, string $nom = '')
    {
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public function sayReady(bool $isHTML = false) {
        $bl = $isHTML ? "<br>\n" : "\n";
        $phrase = $this->prenom;
        $className = get_class($this);
        switch($className) {
            case 'EditeurLogiciel\Developpeur':
                $phrase.='[Dev]';
                break;
        }

        $phrase .= $bl;
        echo $phrase;
    }

    /*
    public function createDeveloppeur($nom, $prenom, $langage) {
        $develope new Developpeur();
    }*/
}
