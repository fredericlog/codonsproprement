<?php
//https://gitlab.com/stromeety/stromeety/-/blob/dev/src/Controller/SecurityApiController.php

namespace App\Controller;

use App\Entity\User;
use App\Service\UserService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

class SecurityApiController extends AbstractController
{
    /**
     * @Route("/auth", name="api_auth", methods={"GET"})
     */
    public function apiAuthentication(UserService $userService, Request $request): JsonResponse
    {
        /** @var ?string $email */
        $email = $request->get('email');
        /** @var ?string $password */
        $password = $request->get('password');

        $userData = [];

        if ((null === $email) || (null === $password)) {
            $error =  [
                'code' => 'EMAIL_OR_PASSWORD_IS_EMPTY',
                'message' => 'Le mot de passe ou email est vide'
            ];
            return new JsonResponse($error, 404);
        }
        $password = base64_decode($request->get('password'));
        $user = $userService->getUserByEmailAndPassword($email, $password);
        if ($user === null) {
            $error =  [
                'code' => 'EMAIL_OR_PASSWORD_IS_UNVAILABLE',
                'message' => 'Le mot de passe ou email est invalide'
            ];
            return new JsonResponse($error, 404);
        }

        $userVerified = $user->isVerified();
        if ($userVerified == 0) {
            $error =  [
                'code' => 'USER_IS_NOT_VERIFIED',
                'message' => 'Votre compte n\'est pas vérifié.'
            ];
            return new JsonResponse($error, 404);
        }

        $name = $user->getName();
        $firstname = $user->getFirstname();
        $email = $user->getEmail();
        $organization = [];
        foreach ($user->getMembership() as $membership) {
            $organization[] = [
                'id' => $membership->getOrganization()->getId(),
                'name' => $membership->getOrganization()->getName()
            ];
        }
        $isAdminData = $user->getRoles();

        $newToken = md5(uniqid($email, true));
        $user->setApiToken($newToken);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        if (empty($isAdminData)) {
            $isAdmin = false;
        } elseif ($isAdminData[0] == 'ROLE_ADMIN') {
            $isAdmin = true;
        } else {
            $isAdmin = false;
        }

        $userData['name'] = $name;
        $userData['firstname'] = $firstname;
        $userData['email'] = $email;
        $userData['token'] = $newToken;
        $userData['isAdmin'] = $isAdmin;
        $userData['organization'] = $organization;

        return new JsonResponse($userData, 200);
    }

    /**
     * @Route("/password_reset", name="api_password_reset", methods={"GET"})
     * @return JsonResponse
     */
    public function resetPassword(
        Request $request,
        MailerInterface $mailer
    ) {
        $email = $request->query->get('email');
        $user = $this->getDoctrine()->getRepository(User::class)
            ->findOneBy(['email' => $email]);
        if (!$user) {
            return new JsonResponse(['error' => 'Pas de compte associé à l\'adresse mail: ' . $email], 403);
        }

        $newToken = md5(uniqid((string)$email, true));
        $user->setApiToken($newToken);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        $email = (new TemplatedEmail())
            ->from(new Address('noreply@stromeety.com', 'Stromeety Support'))
            ->to((string)$email)
            ->subject('Réinitialiser le mot de passe')
            ->htmlTemplate('mailing/password_reset.html.twig')
            ->context(
                [
                    "link" => $_SERVER['URL_FRONT'] . "password_reset?token=" . $newToken,
                    "firstname" => $user->getFirstname(),
                    "name" =>  $user->getName()
                ]
            );
        $mailer->send($email);
        return new JsonResponse("Email send", 200);
    }
}
