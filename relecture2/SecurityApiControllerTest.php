<?php
// https://gitlab.com/stromeety/stromeety/-/blob/dev/tests/Controller/SecurityApiControllerTest.php
namespace App\Tests\Controller;

use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Contracts\HttpClient\ResponseInterface;

class SecurityApiControllerTest extends TestCase
{
    private $urls = [
        [
            'url' => 'http://backapi/auth',
            'code' => 404,
            'message' => 'Le mot de passe ou email est vide'
        ],
        [
            'url' => 'http://backapi/auth?email=maxence.pellerin@epitech.eu&password=bG9naXBybw',
            'code' => 404,
            'message' => 'Le mot de passe ou email est invalide'
        ],
        [
            'url' => 'http://backapi/auth?email=testu.unverified@logipro.com&password=emFFOTg',
            'code' => 404,
            'message' => 'Votre compte n\'est pas vérifié.'
        ],
        [
            'url' => 'http://backapi/auth?email=testu.verified@logipro.com&password=MnZMMmI',
            'code' => 200,
            'firstname' => 'verified',
            'name' => 'testu'
        ]
    ];

    public function testEmptyEmail(): void
    {
        $index = 0;

        $client = new CurlHttpClient(["verify_peer"=>false, "verify_host"=>false]);
        $response = $client->request(
            'GET',
            $this->urls[$index]['url']
        );
        $this->throwAssertException($index, __FUNCTION__, $response);
    }

    public function testWrongCredentials(): void
    {
        $index = 1;

        $client = new CurlHttpClient(["verify_peer"=>false, "verify_host"=>false]);
        $response = $client->request(
            'GET',
            $this->urls[$index]['url']
        );
        $this->throwAssertException($index, __FUNCTION__, $response);
    }

    public function testVerified(): void
    {
        $index = 2;

        $client = new CurlHttpClient(["verify_peer"=>false, "verify_host"=>false]);
        $response = $client->request(
            'GET',
            $this->urls[$index]['url']
        );
        $this->throwAssertException($index, __FUNCTION__, $response);
    }

    public function testValid(): void
    {
        $index = 3;

        $client = new CurlHttpClient(["verify_peer"=>false, "verify_host"=>false]);
        $response = $client->request(
            'GET',
            $this->urls[$index]['url']
        );
        try {
            $this->assertTrue($response->getStatusCode() === $this->urls[$index]['code']);
            $this->assertTrue(json_decode($response->getContent(false), true)["name"] === $this->urls[$index]['name']);
            $this->assertTrue(json_decode($response->getContent(false), true)["firstname"] === $this->urls[$index]['firstname']);
        } catch (ExpectationFailedException $th) {
            echo("\n\033[01;31mFAILED\033[0m: ".__FUNCTION__."\n");
            throw new ExpectationFailedException($th);
        } catch (ClientException $th) {
            throw new ExpectationFailedException($th);
        }
    }

    private function throwAssertException(int $index, string $function_name, ResponseInterface $response)
    {
        try {
            $this->assertTrue($response->getStatusCode() === $this->urls[$index]['code']);
            $this->assertTrue(json_decode($response->getContent(false), true)["message"] === $this->urls[$index]['message']);
        } catch (ExpectationFailedException $th) {
            echo("\n\033[01;31mFAILED\033[0m: ".$function_name."\n");
            throw new ExpectationFailedException($function_name);
        } catch (ClientException $th) {
            throw new ExpectationFailedException($th);
        }
    }
}
