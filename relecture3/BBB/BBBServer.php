<?php

namespace App\BBB;

/**
 * represente la structure d'une serveur BBB
 */
class BBBServer
{
    /**
     * @var string $url
     */
    private string $url;

    /**
     * @var string $secret
     */
    private string $secret;

    public function __construct(string $url, string $secret)
    {
        $this->url = $url;
        $this->secret = $secret;
    }

    public function getUrl():string
    {
        return $this->url;
    }

    public function getSecret():string
    {
        return $this->secret;
    }
}
