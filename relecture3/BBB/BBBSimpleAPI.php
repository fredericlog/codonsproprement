<?php

namespace App\BBB;

use Exception;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * il semble assez facile de faire une api BBB toute simple.
 * API BBB PHP est complexe avec des objets de partouts
 * avec quelques heures de boulot on pourrait completer cette API
 * pour devenir independant du Bundle...
 */
class BBBSimpleAPI
{

    /**
     * url de l'api du serveur BBB
     */
    private string $host;

    /**
     * shared secret du serveur BBB
     */
    private string $secret;

    /**
     * prefix des meetingID générés
     */
    private string $prefix;

    /**
     * meetingID de la conférence
     */
    private string $meetingID;

    private HttpClientInterface $client;

    /**
     *
     * @param string $host BBB URL API ex.: https://webconf-sirius.logipro.com/bigbluebutton/api
     * @param string $secret shared secret fourni par le serveur BBB
     * @param string $prefix un prefix pour meetingID
     * @return void
     */
    public function __construct(string $host, string $secret, string $prefix = 'stromeety_')
    {
        $this->host = $host;
        $this->secret = $secret;
        $this->client = HttpClient::create();
        $this->prefix = $prefix;
        $this->meetingID = '';
    }

    /**
     * @param string $meetingID met à jour le meetingID
     */
    public function setMeetingID(string $meetingID): void
    {
        $this->meetingID = $meetingID;
    }

    /**
     * @return string le meetingID
     */
    public function getMeetingID(): string
    {
        return $this->meetingID;
    }

    /**
     * prepare l'url pour soliciter l'api comme décrit https://docs.bigbluebutton.org/dev/api.html#usage
     * @param string $APICallName nom parmi ceux définits par BBB, ex.: create, join
     * @param array<string,string> $args
     * @return string
     */
    private function prepareURL(string $APICallName, array $args):string
    {
        $url = $this->host.'/'.$APICallName;
        $query = http_build_query($args);
        $sha = sha1($APICallName.$query.$this->secret);
        $url = $url.'?'.$query.'&checksum='.$sha;
        return $url;
    }

    /**
     * create a BBB meeting
     * @param string $name un nom lisible par un humain
     * @param array<string,string> $options toutes les options facultatives proposées par BBB ici :
     *  https://docs.bigbluebutton.org/dev/api.html#create
     * @return string le compte rendu BBB en xml
     */
    public function create(string $name, array $options = array()):string
    {
        if ($this->meetingID == '') {
            $this->meetingID = isset($options['meetingID']) ? $options['meetingID'] : uniqid($this->prefix);
        }

        $arguments = array_merge(['name' => $name, 'meetingID' => $this->meetingID], $options);

        $content = $this->callBBBServer('create', 'GET', $arguments);
 
        return $content;
    }

    public function getMeetingInfo(): string
    {
        if ($this->meetingID == '') {
            throw new Exception('no meetingID : please, create meeting before asking Meeting Info');
        }

        $arguments = [ 'meetingID' => $this->meetingID];

        $content = $this->callBBBServer('getMeetingInfo', 'GET', $arguments);

        return $content;
    }

    /**
     * construit l'url qui DOIT être lancé par le navigateur
     * makeJoinUrl ne fait  donc pas de requete vers le serveur BBB : c'est une URL cliquable, uniquement!
     * @param string $fullName nom de l'utilisateur
     * @param string $password le mot de passe (il determine le role : attendee ou moderator)
     * @param array<string,string> $options les paramètres
     * optionnels (src: https://docs.bigbluebutton.org/dev/api.html#join)
     * @return string message retourné par l'API
     */
    public function makeJoinUrl(string $fullName, string $password, array $options = array()): string
    {
        if ($this->meetingID == '') {
            throw new Exception('no meetingID : please, create meeting before joining');
        }

        $arguments = array_merge(
            [
            'meetingID' => $this->meetingID,
            'fullName' => $fullName,
            'password' => $password
            ],
            $options
        );

        $url = $this->prepareURL('join', $arguments);
        return $url;
    }

    /**
     * @param string $functionName
     * @param string $method
     * @param array<string,string> $args
     * @return string resultat de la requete au serveur BBB
     */
    private function callBBBServer(string $functionName, string $method, array $args): string
    {
        $url = $this->prepareURL($functionName, $args);
        $response =  $this->client->request($method, $url);
        $content = $response->getContent();
        return $content;
    }
}
