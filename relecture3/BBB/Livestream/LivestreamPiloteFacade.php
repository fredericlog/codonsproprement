<?php

namespace App\BBB\Livestream;

interface LivestreamPiloteFacade
{
    public function createLivestreamPod(string $podId):void;
    public function deleteLivestreamPod(string $podId):void;
}
