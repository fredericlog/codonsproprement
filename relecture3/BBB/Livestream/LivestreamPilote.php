<?php

namespace App\BBB\Livestream;

use App\BBB\Livestream\LivestreamPiloteFacade;

class LivestreamPilote implements LivestreamPiloteFacade
{
    private string $BBBServerUrl;
    private string $BBBServerSecret;

    private string $BBBAttendeePassword;
    private string $BBBModeratorPassword;
    private string $meetingID;
    /** @var array<string,string> $options */
    private array $options;

    private string $streamerUrl;

    private string $lastOutput;

    public function __construct()
    {
        $this->lastOutput = '';
    }

    public function setBBBServer(string $BBBServerUrl, string $BBBServerSecret):void
    {
        $this->BBBServerUrl = $BBBServerUrl;
        $this->BBBServerSecret = $BBBServerSecret;
    }

    /**
    * @param string $meetingID
    * @param string $BBBModeratorPassword
    * @param string $BBBAttendeePassword
    * @param array<string,string> $options
    */
    public function setMeeting(
        string $meetingID,
        string $BBBModeratorPassword,
        string $BBBAttendeePassword,
        array $options
    ):void {
        $this->meetingID = $meetingID;
        $this->BBBAttendeePassword= $BBBAttendeePassword;
        $this->BBBModeratorPassword = $BBBModeratorPassword;
        $this->options = $options;
    }

    public function getLastOutput():string
    {
        return $this->lastOutput;
    }

    /**
     * @param string $streamerUrl provided by streamers (ex: rtmp://a.rtmp.youtube.com/live2/vaxw-yfqh-h3jy-cma6-7tfy)
     */
    public function setStreamer(string $streamerUrl):void
    {
        $this->streamerUrl = $streamerUrl;
    }

    /**
     * @param string $podId
     */
    public function createLivestreamPod(string $podId):void
    {
        $pod = $this->formatPodInstructions($podId);

        $stream = new KubernetesDriver();
        $output = $stream->kubectlApply($pod);

        $this->lastOutput = $output;
    }

    /**
     * @return string
     */
    public function getLivestreamPods():string
    {
        $stream = new KubernetesDriver();
        $output = $stream->kubectGet();
        $this->lastOutput = $output;
        return $output;
    }

    /**
     * @param string $podId
     */
    public function deleteLivestreamPod(string $podId):void
    {
        $stream = new KubernetesDriver();
        $output = $stream->kubectDelete('pod', $podId);

        $this->lastOutput = $output;
    }

    /**
     * @param string $podId
     * @return string description du pod en yaml
     */
    private function formatPodInstructions(
        string $podId
    ):string {
        $title = isset($this->options['BBB_MEETING_TITLE']) ? $this->options['BBB_MEETING_TITLE']:'liveStreaming Test';
        $pod = <<< EOS
      apiVersion: v1
      kind: Pod
      metadata:
        name: $podId
        labels:
          app: bbb-stream
      spec:
        containers:
        - name: bbb-stream
          image: registry.gitlab.com/stromeety/livestream/bbb-stream:latest
          env:
          - name: BBB_URL
            value: $this->BBBServerUrl
          - name: BBB_SECRET
            value: $this->BBBServerSecret
          - name: BBB_MEETING_ID
            value: $this->meetingID
          - name: BBB_ATTENDEE_PASSWORD
            value: $this->BBBAttendeePassword
          - name: BBB_MODERATOR_PASSWORD
            value: $this->BBBModeratorPassword
          - name: BBB_MEETING_TITLE
            value: $title
          - name: BBB_STREAM_URL
            value: $this->streamerUrl
        imagePullSecrets:
        - name: regcred
      EOS;
        return $pod;
    }
}
