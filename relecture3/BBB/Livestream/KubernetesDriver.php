<?php

namespace App\BBB\Livestream;

class KubernetesDriver
{
    private string $kubectlPath;
    private string $configFile;

    public function __construct()
    {
        $this->kubectlPath = __DIR__.'/../../../bin/kubectl';
        $this->configFile = __DIR__.'/../../../bin/kubectlconfig.yml';
    }

    /**
     * https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#version
     * @param string $flags (client output short etc)
     * @return string message renvoyé par kubectl
     *
     */
    public function getKubectlClientVersion(string $flags = ''):string
    {
        $command = 'version '.$flags;
        $output = $this->constructAndExecuteCommand($command);
        return $output;
    }

    /**
     * https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply
     * @param string $yamlApplyContent
     * @return string message renvoyé par kubectl
     */
    public function kubectlApply(string $yamlApplyContent):string
    {
        $command = "echo '$yamlApplyContent' | ";
        $apply = 'apply -f -';
        $command.= $this->constructCommand($apply);
        $output = $this->tryExecuteCommand($command);
        return $output;
    }

    /**
     * permet d'executer la commande "kubectl delete" telle que decrite ici:
     * https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#delete
     * @param string $type pod, service, deployement etc.
     * @param string $name
     * @param string $label
     * @return string message renvoyé par kubectl
     */
    public function kubectDelete(string $type, string $name = '', string $label = ''):string
    {
        $delete = "delete $type $name";
        if ($label!='') {
            $delete.=" -l $label";
        }
        $output = $this->constructAndExecuteCommand($delete);
        return $output;
    }

    /**
     * permet d'executer la commande "kubectl get" telle que decrite ici:
     * https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#get
     * @return string retourne la liste des pods
     */
    public function kubectGet():string
    {
        $get = "get pods";
        try {
            $output = $this->constructAndExecuteCommand($get);
        } catch (\Exception $e) {
            return "No resources found in default namespace.";
        }
        return $output;
    }

    private function constructAndExecuteCommand(string $shortCommand):string
    {
        $command = $this->constructCommand($shortCommand);
        $output = $this->tryExecuteCommand($command);
        return $output;
    }

    /**
     * assurez vous de bien avoir construit la command avant l'execution
     * @param string $commandLine nom du script, parametre de config et commannde kubectl
     * @return string ce que renvoie le shell après execution de la commande
     */
    private function tryExecuteCommand(string $commandLine):string
    {
        $output = shell_exec($commandLine);
        if (($output===false) || ($output == null)) {
            throw new \Exception('Bad command sent to kubectl');
        }
        return $output;
    }

    private function constructCommand(string $shortCommand):string
    {
        $command = $this->kubectlPath.' ';
        $command.= $shortCommand;
        $command.= ' --kubeconfig '.$this->configFile;
        return $command;
    }
}
