<?php

namespace App\BBB;

use BigBlueButton\Core\Meeting;
use BigBlueButton\Core\Attendee;

/**
 * enveloppe de Meeting
 */
class BBBMeeting extends Meeting
{
    /** @var Meeting */
    private $meeting = null;

    public function __construct(Meeting $meeting)
    {
        $this->meeting = $meeting;
    }

    /**
     * @return string
     */
    public function getMeetingId()
    {
        return $this->meeting->getMeetingId();
    }

    /**
     * @return string
     */
    public function getMeetingName()
    {
        return $this->meeting->getMeetingName();
    }

    /**
     * @return double
     */
    public function getCreationTime()
    {
        return $this->meeting->getCreationTime();
    }

    /**
     * @return string
     */
    public function getCreationDate()
    {
        return $this->meeting->getCreationDate();
    }

    /**
     * @return int
     */
    public function getVoiceBridge()
    {
        return $this->meeting->getVoiceBridge();
    }

    /**
     * @return string
     */
    public function getDialNumber()
    {
        return $this->meeting->getDialNumber();
    }

    /**
     * @return string
     */
    public function getAttendeePassword()
    {
        return $this->meeting->getAttendeePassword();
    }

    /**
     * @return string
     */
    public function getModeratorPassword()
    {
        return $this->meeting->getModeratorPassword();
    }

    /**
     * @return bool
     */
    public function hasBeenForciblyEnded()
    {
        return $this->meeting->hasBeenForciblyEnded();
    }

    /**
     * @return bool
     */
    public function isRunning()
    {
        return $this->meeting->isRunning();
    }

    /**
     * @return int
     */
    public function getParticipantCount()
    {
        return $this->meeting->getParticipantCount();
    }

    /**
     * @return int
     */
    public function getListenerCount()
    {
        return $this->meeting->getListenerCount();
    }

    /**
     * @return int
     */
    public function getVoiceParticipantCount()
    {
        return $this->meeting->getVoiceParticipantCount();
    }

    /**
     * @return int
     */
    public function getVideoCount()
    {
        return $this->meeting->getVideoCount();
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->meeting->getDuration();
    }

    /**
     * @return bool
     */
    public function hasUserJoined()
    {
        return $this->meeting->hasUserJoined();
    }

    /**
     * @return string
     */
    public function getInternalMeetingId()
    {
        return $this->meeting->getInternalMeetingId();
    }

    /**
     * @return bool
     */
    public function isRecording()
    {
        return $this->meeting->isRecording();
    }

    /**
     * @return double
     */
    public function getStartTime()
    {
        return $this->meeting->getStartTime();
    }

    /**
     * @return double
     */
    public function getEndTime()
    {
        return $this->meeting->getEndTime();
    }

    /**
     * @return int
     */
    public function getMaxUsers()
    {
        return $this->meeting->getMaxUsers();
    }

    /**
     * @return int
     */
    public function getModeratorCount()
    {
        return $this->meeting->getModeratorCount();
    }

    /**
     * @return Attendee[]
     */
    public function getAttendees():array
    {
        return $this->meeting->getAttendees();
    }

    /**
     * Moderators of Meeting - Subset of Attendees
     * @return Attendee[]
     */
    public function getModerators():array
    {
        return $this->meeting->getModerators();
    }

    /**
     * Viewers of Meeting - Subset of Attendees
     * @return Attendee[]
     */
    public function getViewers():array
    {
        return $this->meeting->getViewers();
    }

    /**
     * @return array<string,string>
     */
    public function getMetas():array
    {
        return $this->meeting->getMetas();
    }
}
