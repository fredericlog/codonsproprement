<?php

namespace App\BBB;

use App\BBB\BigBlueButtonFacade;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\DeleteRecordingsParameters;
use BigBlueButton\Parameters\EndMeetingParameters;
use BigBlueButton\Parameters\GetMeetingInfoParameters;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use BigBlueButton\Parameters\SetConfigXMLParameters;
use BigBlueButton\Responses\DeleteRecordingsResponse;
use BigBlueButton\Responses\GetDefaultConfigXMLResponse;
use BigBlueButton\Responses\JoinMeetingResponse;
use SimpleXMLElement;

/**
 * BBBApi implement les design pattern  envelope + singleton :
 * 1) l'envelope : l'API BigBluButton est encapsulé dans cet objet => personne n'a besoin de l'utiliser
 * ailleurs dans le code. Et aussi ca permet de faire des fonctions statiques plus respectueuses
 * de l'API de base : https://docs.bigbluebutton.org/dev/api.html
 *
 * 2) le singleton : pour eviter la double instanciations vers un serveur BBB
 */
class BBBApi
{
    /**
     * @var BBBApi
     */
    private static $instance = null;

    /**
     * @var BigBlueButtonFacade
     */
    private $bbb = null;

    private function __construct()
    {
    }

    /**
     * unique instance de la classe
     * @return BBBApi
     */
    public static function getInstance():BBBApi
    {
        if (self::$instance == null) {
            self::$instance = new BBBApi();
        }
        return self::$instance;
    }

    /**
     * @param string $host BBB URL API ex.: https://webconf-sirius.logipro.com/bigbluebutton/api
     * @param string $secret shared secret fourni par le serveur BBB
     */
    public static function addHost(string $host, string $secret):void
    {
        $bbbapi = self::getInstance();

        if ($bbbapi->bbb == null) {
            $bbbapi->bbb = new BigBlueButtonFacade($host, $secret);
        }
    
        return;
    }

    /**
     * @return string
     */
    public function getJSessionId()
    {
        return $this->bbb->getJSessionId();
    }

    /**
     *
     *
     * @param string $meetingID
     * @param string $meetingName correspond au parametre 'name'
     * @param array<string,string> $options les parametres
     * décrits ici https://docs.bigbluebutton.org/dev/api.html#create
     * @param array<string,string> $files key = filename.extension,  value => content,url
     * @return \SimpleXMLElement retour de l'API BBB native https://docs.bigbluebutton.org/dev/api.html#create
     */
    public function create(
        string $meetingID,
        string $meetingName,
        array $options,
        array $files = []
    ):\SimpleXMLElement {
        $options['meetingID'] = $meetingID;
        $options['name'] = $meetingName;
        $createMeetingParams = $this->createCreateMeetingParameters($options, $files);

        $response = $this->bbb->createMeeting($createMeetingParams);
        $returnXML = $response->getRawXml();
        return $returnXML;
    }

    /**
     * @param array<string,string> $p
     * @param array<string,string> $files
     * @return CreateMeetingParameters
     */
    private function createCreateMeetingParameters(array $p, array $files):CreateMeetingParameters
    {
        $meetingID = $p['meetingID'];
        $meetingName = $p['name'];
        $c = new CreateMeetingParameters($meetingID, $meetingName);

        foreach ($files as $filename => $value) {
            $nameOrUrl = $filename;
            $content = $value;
            /** @var null $content */ // hérésie due au typage erronné  @param null $content et @param null $filename
            $c->addPresentation($nameOrUrl, $content);
        }
        isset($p['moderatorPW']) ? $c->setModeratorPassword($p['moderatorPW']):null;
        isset($p['attendeePW']) ? $c->setAttendeePassword($p['attendeePW']):null;
        isset($p['dialNumber']) ? $c->setDialNumber($p['dialNumber']):null;
        isset($p['voiceBridge']) ? $c->setVoiceBridge((int)$p['voiceBridge']):null;
        isset($p['webVoice']) ? $c->setWebVoice($p['webVoice']):null;
        isset($p['logoutURL']) ? $c->setLogoutUrl($p['logoutURL']):null;
        isset($p['record']) ? $c->setRecord((bool)$p['record']):null;
        isset($p['duration']) ? $c->setDuration((int)$p['duration']):null;
        isset($p['maxParticipants']) ? $c->setMaxParticipants((int)$p['maxParticipants']):null;
        isset($p['autoStartRecording']) ? $c->setAutoStartRecording((bool)$p['autoStartRecording']):null;
        isset($p['allowStartStopRecording']) ? $c->setAllowStartStopRecording((bool)$p['allowStartStopRecording']):null;
        isset($p['welcome']) ? $c->setWelcomeMessage($p['welcome']):null;
        isset($p['moderatorOnlyMessage']) ? $c->setModeratorOnlyMessage($p['moderatorOnlyMessage']):null;
        isset($p['webcamsOnlyForModerator']) ? $c->setWebcamsOnlyForModerator((bool)$p['webcamsOnlyForModerator']):null;
        isset($p['logo']) ? $c->setLogo($p['logo']):null;
        isset($p['copyright']) ? $c->setCopyright($p['copyright']):null;
        isset($p['muteOnStart']) ? $c->setMuteOnStart((bool)$p['muteOnStart']):null;
        isset($p['guestPolicy']) ? $c->setGuestPolicy((bool)$p['guestPolicy']):null;
        isset($p['lockSettingsDisableCam']) ? $c->setLockSettingsDisableCam((bool)$p['lockSettingsDisableCam']):null;
        isset($p['lockSettingsDisableMic']) ? $c->setLockSettingsDisableMic((bool)$p['lockSettingsDisableMic']):null;
        isset($p['lockSettingsDisablePrivateChat']) ?
            $c->setLockSettingsDisablePrivateChat((bool)$p['lockSettingsDisablePrivateChat']):null;
        isset($p['lockSettingsDisableNote']) ? $c->setLockSettingsDisableNote((bool)$p['lockSettingsDisableNote']):null;
        isset($p['lockSettingsHideUserList']) ?
            $c->setLockSettingsHideUserList((bool)$p['lockSettingsHideUserList']):null;
        isset($p['lockSettingsLockedLayout']) ?
            $c->setLockSettingsLockedLayout((bool)$p['lockSettingsLockedLayout']):null;
        isset($p['lockSettingsLockOnJoin']) ? $c->setLockSettingsLockOnJoin((bool)$p['lockSettingsLockOnJoin']):null;
        isset($p['lockSettingsLockOnJoinConfigurable']) ?
            $c->setLockSettingsLockOnJoinConfigurable((bool)$p['lockSettingsLockOnJoinConfigurable']):null;
        isset($p['allowModsToUnmuteUsers']) ? $c->setAllowModsToUnmuteUsers((bool)$p['allowModsToUnmuteUsers']):null;
        isset($p['allowStartStopRecording']) ? $c->setAllowStartStopRecording((bool)$p['allowStartStopRecording']):null;
        isset($p['autoStartRecording']) ? $c->setAutoStartRecording((bool)$p['autoStartRecording']):null;

        return $c;
    }
    /**
     * 2 parametres obligatoires et 2 seulement. https://docs.bigbluebutton.org/dev/api.html#end
     * @param string $meetingID
     * @param string $moderatorPassword
     * @return \SimpleXMLElement comme décrit ici https://docs.bigbluebutton.org/dev/api.html#end
     */
    public function end(string $meetingID, string $moderatorPassword):\SimpleXMLElement
    {
        $endMeetingParams = $this->createEndMeetingParameters($meetingID, $moderatorPassword);

        $response = $this->bbb->endMeeting($endMeetingParams);
        $returnXML = $response->getRawXml();
        return $returnXML;
    }

    private function createEndMeetingParameters(string $meetingID, string $moderatorPassword):EndMeetingParameters
    {
        $e = new EndMeetingParameters($meetingID, $moderatorPassword);
        return $e;
    }

    /**
     * https://docs.bigbluebutton.org/dev/api.html#ismeetingrunning
     * @param string $meetingID
     * @return \SimpleXMLElement
     */
    public function isMeetingRunning(string $meetingID):\SimpleXMLElement
    {
        $m = new IsMeetingRunningParameters($meetingID);
        $response = $this->bbb->isMeetingRunning($m);
        $returnXML = $response->getRawXml();
        return $returnXML;
    }
    /**
     * https://docs.bigbluebutton.org/dev/api.html#getmeetinginfo
     * @param string $meetingID
     * @return \SimpleXMLElement comme décrit ici https://docs.bigbluebutton.org/dev/api.html#getmeetinginfo
     */
    public function getMeetingInfo(string $meetingID, string $moderatorPassword):\SimpleXMLElement
    {
        $m = new GetMeetingInfoParameters($meetingID, $moderatorPassword);
        $response = $this->bbb->getMeetingInfo($m);
        $returnXML = $response->getRawXml();
        return $returnXML;
    }

    /**
     *
     * @param string $meetingID
     * @param string $fullName le nom de l'utilisateur
     * @param string $password le mot de passe moderateur ou attendee
     * @param array<string,string> $options les parametres décrits ici https://docs.bigbluebutton.org/dev/api.html#join
     * @return string url du meeting
     */
    public function joinWithRedirect(
        string $meetingID,
        string $fullName,
        string $password,
        array $options
    ):string {
        $options['meetingID']=$meetingID;
        $options['fullName']=$fullName;
        $options['password']=$password;
        $options['redirect']='true';
        $joinMeetingParams = $this->formatJoinMeetingParameters($options);
        $urlMeeting = $this->bbb->getJoinMeetingURL($joinMeetingParams);
        return $urlMeeting;
    }

    /**
     *
     * @param string $meetingID
     * @param string $fullName le nom de l'utilisateur
     * @param string $password le mot de passe moderateur ou attendee
     * @param array<string,string> $options les parametres décrits ici https://docs.bigbluebutton.org/dev/api.html#join
     * @return SimpleXMLElement reponse du join
     */
    public function join(
        string $meetingID,
        string $fullName,
        string $password,
        array $options
    ):\SimpleXMLElement {
        $options['meetingID']=$meetingID;
        $options['fullName']=$fullName;
        $options['password']=$password;
        $options['redirect']='false';
        $joinMeetingParams = $this->formatJoinMeetingParameters($options);
        $joinMeeting = $this->bbb->joinMeeting($joinMeetingParams);
        $returnXML = $joinMeeting->getRawXml();
        return $returnXML;
    }

    /**
     * @param array<string,string> $p
     * @return JoinMeetingParameters
     */
    private function formatJoinMeetingParameters(array $p):JoinMeetingParameters
    {
        $meetingID = $p['meetingID'];
        $username = $p['fullName'];
        $password = $p['password'];
        $j = new JoinMeetingParameters($meetingID, $username, $password);
        isset($p['createTime']) ? $j->setCreationTime((int)$p['createTime']):null;
        isset($p['userID']) ? $j->setUserId($p['userID']):null;
        isset($p['webVoiceConf']) ? $j->setWebVoiceConf($p['webVoiceConf']):null;
        isset($p['configToken']) ? $j->setConfigToken($p['configToken']):null;
        //isset($p['defaultLayout']) ? $j->set??????($p['defaultLayout']):null;
        isset($p['avatarURL']) ? $j->setAvatarURL($p['avatarURL']):null;
        isset($p['redirect']) ? $j->setRedirect($this->formatRedirect($p['redirect'])):null;
        isset($p['clientURL']) ? $j->setClientURL($p['clientURL']):null;
        isset($p['joinViaHtml5']) ? $j->setJoinViaHtml5((bool)$p['joinViaHtml5']):null;
        //isset($p['guest']) ? $j->set????($p['guest']):null;
        return $j;
    }

    private function formatRedirect(string $redirect):bool
    {
        if ($redirect == 'false') {
            return false;
        }
        return true;
    }

    /**
     * https://docs.bigbluebutton.org/dev/api.html#getdefaultconfigxml
     * @return \SimpleXMLElement comme décrit ici https://docs.bigbluebutton.org/dev/api.html#getdefaultconfigxml
     * @deprecated fonctionne uniquement pour le client flash qui n'est plus soutenu
     */
    public function getDefaultConfigXML():\SimpleXMLElement
    {
        $response = $this->bbb->getDefaultConfigXML();
        $returnXML = $response->getRawXml();
        return $returnXML;
    }
    /**
     * @param string $meetingID
     * @param \SimpleXMLElement $configXML
     * @return \SimpleXMLElement comme décrit ici https://docs.bigbluebutton.org/dev/api.html#end
     * @deprecated fonctionne uniquement pour le client flash qui n'est plus soutenu
     */
    public function setConfigXML(string $meetingID, \SimpleXMLElement $configXML):\SimpleXMLElement
    {
        $setConfigParams = new SetConfigXMLParameters($meetingID);
        $setConfigParams->setRawXml($configXML);
        $response = $this->bbb->setConfigXML($setConfigParams);
        $returnXML = $response->getRawXml();
        return $returnXML;
    }

    /**
     * https://docs.bigbluebutton.org/dev/api.html#getrecordings
     * @param array<string,string> $options comme décrit ici https://docs.bigbluebutton.org/dev/api.html#getrecordings
     * @return \SimpleXMLElement
     */
    public function getRecordings(array $options):\SimpleXMLElement
    {
        $r = $this->createGetRecordingsParameters($options);
        $response = $this->bbb->getRecordings($r);
        $returnXML = $response->getRawXml();
        return $returnXML;
    }

    /**
     * @param array<string,string> $p
     * @return GetRecordingsParameters
     */
    private function createGetRecordingsParameters(array $p):GetRecordingsParameters
    {
        $r = new GetRecordingsParameters();
        isset($p['meetingID']) ? $r->setMeetingId($p['meetingID']):null;
        isset($p['recordID']) ? $r->setRecordId($p['recordID']):null;
        isset($p['state']) ? $r->setState($p['state']):null;
        if (isset($p['meta'])) {
            /** @var array<string,string> $metas */
            $metas = $p['meta'];
            foreach ($metas as $key => $v) {
                $r->addMeta($key, $v);
            }
        }
        return $r;
    }

    /**
     * https://docs.bigbluebutton.org/dev/api.html#deleterecordings
     * @param string $recordID
     * @return \SimpleXMLElement
     */
    public function deleteRecordings(string $recordID):\SimpleXMLElement
    {
        $r = $this->createDeleteRecordingsParameters($recordID);
        $response = $this->bbb->deleteRecordings($r);
        $returnXML = $response->getRawXml();
        return $returnXML;
    }

    private function createDeleteRecordingsParameters(string $recordID):DeleteRecordingsParameters
    {
        $r = new DeleteRecordingsParameters($recordID);
        return $r;
    }
}
