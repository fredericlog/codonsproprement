<?php

namespace App\BBB;

use BigBlueButton\BigBlueButton;
use BigBlueButton\Responses\JoinMeetingResponse;
use BigBlueButton\Responses\IsMeetingRunningResponse;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use BigBlueButton\Util\UrlBuilder;
use BigBlueButton\Exceptions\BadResponseException;
use SimpleXMLElement;
use CurlHandle;

class BigBlueButtonFacade extends BigBlueButton
{

    /**
     * copy/cut of BigBlueButton constructor because type param error
     * @param string $baseUrl
     * @param string $secret
     */
    public function __construct($baseUrl = null, $secret = null)
    {
        // Keeping backward compatibility with older deployed versions
        // BBB_SECRET is the new variable name and have higher priority against the old named BBB_SECURITY_SALT
        $this->securitySecret   = $secret ?: getenv('BBB_SECRET') ?: getenv('BBB_SECURITY_SALT');
        $this->bbbServerBaseUrl = $baseUrl ?: getenv('BBB_SERVER_BASE_URL');
        $this->urlBuilder       = new UrlBuilder($this->securitySecret, $this->bbbServerBaseUrl);
    }

    /** @var array<string,string> */
    private static $cookies;

    /**
     * @param JoinMeetingParameters $joinMeetingParams
     * @return JoinMeetingResponse
     * @throws \RuntimeException
     */
    public function joinMeeting($joinMeetingParams)
    {
        $xml = $this->processXmlResponseFixed($this->getJoinMeetingURL($joinMeetingParams));

        return new JoinMeetingResponse($xml);
    }

    /**
     * @param IsMeetingRunningParameters $meetingParams
     * @return IsMeetingRunningResponse
     * @throws \RuntimeException
     */
    public function isMeetingRunning($meetingParams)
    {
        $xml = $this->processXmlResponseFixed($this->getIsMeetingRunningUrl($meetingParams));

        return new IsMeetingRunningResponse($xml);
    }

    /**
     * A private utility method used by other public methods to process XML responses.
     *
     * @param  string            $url
     * @param  string            $payload
     * @param  string            $contentType
     * @return SimpleXMLElement
     * @throws \RuntimeException
     */
    private function processXmlResponseFixed($url, $payload = '', $contentType = 'application/xml')
    {
        if (extension_loaded('curl')) {
            $ch = curl_init();
            //curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_HEADERFUNCTION, "self::curlResponseHeaderCallback");
            if ($ch == false) {
                throw new \RuntimeException('Unhandled curl error: '.$url);
            }
            $timeout = 10;
            if (!empty($this->getJSessionId())) {
                curl_setopt(
                    $ch,
                    CURLOPT_HTTPHEADER,
                    array("Cookie: JSESSIONID=".$this->getJSessionId())
                );
            }
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            //curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiefilepath);
            //curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiefilepath);
            if (!empty($payload)) {
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-type: ' . $contentType,
                    'Content-length: ' . mb_strlen($payload),
                ]);
            }
            $data = curl_exec($ch);
            if ($data === false) {
                throw new \RuntimeException('Unhandled curl error: ' . curl_error($ch));
            }
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($httpcode < 200 || $httpcode >= 300) {
                throw new BadResponseException('Bad response, HTTP code: ' . $httpcode);
            }
            curl_close($ch);
            /*
            $cookies = file_get_contents($cookiefilepath);
            if (strpos($cookies, 'JSESSIONID') !== false) {
                preg_match('/(?:JSESSIONID\s*)(?<JSESSIONID>.*)/', $cookies, $output_array);
                $this->setJSessionId($output_array['JSESSIONID']);
            }*/
            if (isset(self::$cookies['JSESSIONID'])) {
                $this->setJSessionId(self::$cookies['JSESSIONID']);
            }
            if (is_bool($data)) {
                throw new \Exception("return of a bool instead of string for requested url : $url");
            }
            return new SimpleXMLElement($data);
        } else {
            throw new \RuntimeException('Post XML data set but curl PHP module is not installed or not enabled.');
        }
    }

    private static function curlResponseHeaderCallback(CurlHandle $ch, string $headerLine):int
    {
        if (preg_match('/^Set-Cookie: \s*([^=]*)=([^;]*)/mi', $headerLine, $matches) == 1) {
            self::$cookies[$matches[1]] = $matches[2];
        }
        return strlen($headerLine); // Needed by curl
    }
}
