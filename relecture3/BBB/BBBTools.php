<?php

namespace App\BBB;

use App\Entity\Event;
use Exception;

class BBBTools
{
    const EVENT_STAT_URL_PORT = '59999';

    /**
     * @param BBBApi $BBBApi
     * @param Event $event
     * @param string $fullName
     * @param string $password
     * @param array<string,string> $options
     * @return \SimpleXMLElement
     */
    public static function joinBBBFromEvent(
        BBBApi $BBBApi,
        Event $event,
        string $fullName,
        string $password,
        array $options
    ):\SimpleXMLElement {
        $xmlJoin = self::tryJoin($BBBApi, $event->getBbbMeetingID(), $fullName, $password, $options);

        $serverUrl = self::formatServerUrl($xmlJoin->url);
        $event->setBbbServerUrl($serverUrl);

        return $xmlJoin;
    }
    /**
     * @param BBBApi $BBBApi
     * @param string $meetingId
     * @param string $fullName
     * @param string $password
     * @param array<string,string> $options
     * @return \SimpleXMLElement
     */
    private static function tryJoin(
        BBBApi $BBBApi,
        string $meetingId,
        string $fullName,
        string $password,
        array $options
    ):\SimpleXMLElement {
        try {
            $options['redirect'] = 'false'; // conditions necessaire pour avoir une reponse xml
            $xmlJoin = $BBBApi->join($meetingId, $fullName, $password, $options);
        } catch (\Exception $e) {
            throw new Exception("join error: Meeting ID:".$meetingId." - ".$e->getMessage());
        }

        return $xmlJoin;
    }

    private static function formatServerUrl(string $fullUrl):string
    {
        preg_match('/(https:\/\/[a-zA-Z0-9\-\.]+)\/(.+)/', $fullUrl, $output_array);
        return $output_array[1];
    }

    public static function getEventStatsUrl(Event $event):string
    {
        $urlWithoutS = str_replace('https', 'http', $event->getBbbServerUrl());
        $url = $urlWithoutS.':'.static::EVENT_STAT_URL_PORT;
        return $url;
    }
}
