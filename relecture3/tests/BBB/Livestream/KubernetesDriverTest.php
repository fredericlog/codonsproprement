<?php

namespace App\Tests\BBB\Livestream;

use App\BBB\Livestream\KubernetesDriver;
use PHPUnit\Framework\TestCase;

class KubernetesDriverTest extends TestCase
{
    public function testGetKubectlClientVersion():void
    {
        $kubectl = new KubernetesDriver();

        $version = $kubectl->getKubectlClientVersion();
        $this->assertStringContainsString('version.Info{Major', $version);
        $this->assertStringContainsString('Client Version:', $version);
        $this->assertStringContainsString('Server Version:', $version);

        $version = $kubectl->getKubectlClientVersion("--client");
        $this->assertStringContainsString('Client Version:', $version);
        $this->assertStringNotContainsString('Server Version:', $version);

        $version = $kubectl->getKubectlClientVersion("--output=json");
        $this->assertStringContainsString('"clientVersion": {', $version);
    }

    public function testKubectlApplyAndKubectlDelete():void
    {
        $kubectl = new KubernetesDriver();

        $filename = __DIR__.'/podtest.yml';
        $yamlPod = file_get_contents($filename);
        if ($yamlPod == false) {
            throw new \Exception('Test: Cannot read '.$filename);
        }
        $createPod = $kubectl->kubectlApply($yamlPod);
        $this->assertStringContainsString('created', $createPod);

        $deleteResult = $kubectl->kubectDelete('pod', 'podtest');
        $this->assertStringContainsString('deleted', $deleteResult);
    }
}
