<?php

namespace App\Tests\BBB\Livestream;

use App\BBB\Livestream\LivestreamPilote;
use PHPUnit\Framework\TestCase;

class LivestreamPiloteTest extends TestCase
{
    public function testCreateLivestreamPod():void
    {
        $stream = new LivestreamPilote();
        $stream->setBBBServer(
            $_SERVER['BBB_HOST'],
            $_SERVER['BBB_SECRET']
        );
        $stream->setMeeting(
            'stromeety_demo', // meeting ID
            'moderator', // moderator password
            'attendee', // attendee password
            []
        );

        $stream->setStreamer(
            'rtmp://a.rtmp.youtube.com/live2/vaxw-yfqh-h3jy-cma6-7tfy'
        );

        $stream->createLivestreamPod("testpod");
        $output = $stream->getLastOutput();
        $this->assertStringContainsString('created', $output);

        $stream->deleteLivestreamPod("testpod");
        $output = $stream->getLastOutput();
        $this->assertStringContainsString('deleted', $output);
    }
}
