<?php

namespace App\Tests\BBB;

use App\BBB\BBBTools;
use App\Entity\Event;
use Symfony\Component\HttpClient\HttpClient;

class BBBToolsTest extends BBBBaseTestCase
{
    public function testJoinBBBFromEvent():void
    {
        $options = array(
            'userID' => 'testJoinBBBFromEvent'
        );

        $event = new Event();
        $event->setBbbMeetingID(self::$meetingID);

        $xmlJoinResult = BBBTools::joinBBBFromEvent(
            self::$BBB,
            $event,
            'Angela Join',
            self::$createParams['moderatorPW'],
            $options
        );

        $this->assertStringStartsWith('SUCCESS', $xmlJoinResult->returncode);
        $this->assertRegExp('/https:\/\/(.*)\/html5client\/join(.*)/', $xmlJoinResult->url);
        $this->assertRegExp('/^https:\/\/[a-zA-Z0-9\-\.]+$/', $event->getBbbServerUrl());
    }

    public function testGetEventStatsUrl():void
    {
        $options = array(
            'userID' => 'testGetEventStatsUrl'
        );

        $event = new Event();
        $event->setBbbMeetingID(self::$meetingID);

        $joinUrl = BBBTools::joinBBBFromEvent(
            self::$BBB,
            $event,
            'Angela Join',
            self::$createParams['moderatorPW'],
            $options
        );

        $urlServerStats = BBBTools::getEventStatsUrl($event);

        $client = HttpClient::create();
        $response = $client->request(
            'POST',
            $urlServerStats,
            [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'body' => json_encode(
                    [
                        'token' => $_SERVER['BBB_EVENT_STATS_SERVER_TOKEN'],
                        'meetingid' => $event->getBbbMeetingID()
                    ]
                )
            ]
        );
        $jsonStats = $response->getContent();
        $this->assertIsString($jsonStats);
    }
}
