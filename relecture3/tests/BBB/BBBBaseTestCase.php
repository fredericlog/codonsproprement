<?php

namespace App\Tests\BBB;

use PHPUnit\Framework\TestCase;
use App\BBB\BBBApi;

class BBBBaseTestCase extends TestCase
{
    
    protected static BBBApi $BBB;

    protected static string $host;

    /** @var array<string,string> */
    protected static array $createParams;

    protected static string $meetingID;
    protected static string $meetingName;

    /**
     * permettra d'utiliser la meme connexion pour tous les tests et de disposer
     * d'un meeting utilisable pour certains tests
     */
    public static function setUpBeforeClass(): void
    {
        self::$host = $_SERVER['BBB_HOST'];
        BBBApi::addHost(
            self::$host,
            $_SERVER['BBB_SECRET']
        );
        self::$BBB = BBBApi::getInstance();

        self::$createParams =array(
            'moderatorPW' => 'pwdmoderator'
        );

        self::$meetingID = 'stromeety_test';
        self::$meetingName = 'MyStromeety';

        $xmlReturnCreate = self::$BBB->create(self::$meetingID, self::$meetingName, self::$createParams);
        self::assertEquals(self::$meetingID, $xmlReturnCreate->meetingID);
    }

    /**
     * a la fin des tests on clos poliment le meeting sinon celui durerait encore quelques
     * temps. Ce qui bloquerait la relance des tests. (Cela peut se produire si le test est interrompu
     * avant la fin)
     */
    public static function tearDownAfterClass(): void
    {
        $xmlReturnEnd = self::$BBB->end(self::$meetingID, self::$createParams['moderatorPW']);
        $someXML = $xmlReturnEnd->asXML();
        self::assertIsNotBool($someXML);
        /** @var string $someXML */
        self::assertStringStartsWith("<?xml", $someXML);
    }
}
