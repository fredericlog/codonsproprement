<?php

namespace App\Tests\BBB;

use App\Tests\BBB\BBBBaseTestCase;
use App\BBB\BBBApi;
use App\BBB\BBBMeeting;
use BigBlueButton\Responses\JoinMeetingResponse;

class BBBApiTest extends BBBBaseTestCase
{
    public function testCreateAndEnd():void
    {
        $meetingToEnd = "MeetingToEndID";
        $meetingToEndName = "Un nom quelconque";

        $xmlReturnCreate = self::$BBB->create($meetingToEnd, $meetingToEndName, self::$createParams);
        $this->assertEquals($meetingToEnd, $xmlReturnCreate->meetingID);

        $xmlReturnEnd = self::$BBB->end($meetingToEnd, self::$createParams['moderatorPW']);
        $someXML = $xmlReturnEnd->asXML();
        $this->assertIsNotBool($someXML);
        /** @var string $someXML */
        $this->assertStringStartsWith("<?xml", $someXML);
    }

    public function testGetMeetingInfo(): void
    {
        $meetingXML = self::$BBB->getMeetingInfo(self::$meetingID, self::$createParams ['moderatorPW']);
        $this->assertEquals(self::$meetingName, $meetingXML->meetingName);
        $this->assertEquals(self::$createParams['moderatorPW'], $meetingXML->moderatorPW);
        $this->assertEquals('false', $meetingXML->running);
    }

    public function testJoinWithRedirect(): void
    {
        $options = array(
            'userID' => '3.14testJoinWithRedirect'
        );

        $urlmeeting = self::$BBB->joinWithRedirect(
            self::$meetingID,
            'Frédéric Royet',
            self::$createParams['moderatorPW'],
            $options
        );

        $this->assertStringStartsWith(self::$host, $urlmeeting);
        $this->assertStringContainsString('checksum', $urlmeeting);
        $this->assertStringContainsString('redirect=true', $urlmeeting);
    }

    public function testJoin(): void
    {
        $options = array(
            'userID' => '3.14JoinWithoutRedirect'
        );

        $joinMeeting = self::$BBB->join(
            self::$meetingID,
            'Sans Redirect',
            self::$createParams['moderatorPW'],
            $options
        );

        $this->assertEquals('SUCCESS', $joinMeeting->returncode, 'Join meeting');
        $this->assertNotEmpty($joinMeeting->message);
        $this->assertNotEmpty($joinMeeting->session_token);
        $this->assertNotEmpty($joinMeeting->auth_token);
        $this->assertNotEmpty($joinMeeting->user_id);
        $this->assertNotEmpty($joinMeeting->url);
        $this->assertNotEmpty(self::$BBB->getJSessionId());
    }

    public function notestIsMeetingRunning(): void
    {
        $options = array(
            'userID' => 'testIsMettingRunning'
        );

        $xmlJoinResponse = self::$BBB->join(
            self::$meetingID,
            'Mohamed IsMettingRunning',
            self::$createParams['moderatorPW'],
            $options
        );

        $this->assertNotEmpty(self::$BBB->getJSessionId());

        $isMeetingRunningXML = self::$BBB->isMeetingRunning(self::$meetingID);
        $this->assertEquals('true', $isMeetingRunningXML->running);
    }
}
